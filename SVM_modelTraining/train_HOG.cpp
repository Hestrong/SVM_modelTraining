#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/ml.hpp"
#include "opencv2/objdetect.hpp"
#include "fstream"

#include <iostream>
#include <time.h>


using namespace cv;
using namespace cv::ml;
using namespace std;

vector< float > get_svm_detector(const Ptr< SVM >& svm);
void convert_to_ml(const std::vector< Mat > & train_samples, Mat& trainData);
void load_images(const String & dirname, vector< Mat > & img_lst, bool showImages);
void sample_neg(const vector< Mat > & full_neg_lst, vector< Mat > & neg_lst, const Size & size);
void computeHOGs(const Size wsize, const vector< Mat > & img_lst, vector< Mat > & gradient_lst, bool use_flip);
void test_trained_detector(String obj_det_filename, String test_dir, String videofilename);

vector< float > get_svm_detector(const Ptr< SVM >& svm)
{
	// get the support vectors
	Mat sv = svm->getSupportVectors();
	const int sv_total = sv.rows;
	// get the decision function
	Mat alpha, svidx;
	double rho = svm->getDecisionFunction(0, alpha, svidx);

	CV_Assert(alpha.total() == 1 && svidx.total() == 1 && sv_total == 1);
	CV_Assert((alpha.type() == CV_64F && alpha.at<double>(0) == 1.) ||
		(alpha.type() == CV_32F && alpha.at<float>(0) == 1.f));
	CV_Assert(sv.type() == CV_32F);

	vector< float > hog_detector(sv.cols + 1);
	memcpy(&hog_detector[0], sv.ptr(), sv.cols * sizeof(hog_detector[0]));
	hog_detector[sv.cols] = (float)-rho;
	return hog_detector;
}

/*
* Convert training/testing set to be used by OpenCV Machine Learning algorithms.
* TrainData is a matrix of size (#samples x max(#cols,#rows) per samples), in 32FC1.
* Transposition of samples are made if needed.
*/
void convert_to_ml(const vector< Mat > & train_samples, Mat& trainData)
{
	//--Convert data
	const int rows = (int)train_samples.size();
	const int cols = (int)std::max(train_samples[0].cols, train_samples[0].rows);
	Mat tmp(1, cols, CV_32FC1); //< used for transposition if needed
	trainData = Mat(rows, cols, CV_32FC1);

	for (size_t i = 0; i < train_samples.size(); ++i)
	{
		CV_Assert(train_samples[i].cols == 1 || train_samples[i].rows == 1);

		if (train_samples[i].cols == 1)
		{
			transpose(train_samples[i], tmp);
			tmp.copyTo(trainData.row((int)i));
		}
		else if (train_samples[i].rows == 1)
		{
			train_samples[i].copyTo(trainData.row((int)i));
		}
	}
}

void load_images(const String & dirname, vector< Mat > & img_lst, bool showImages = false)
{
	vector< String > files;
	glob(dirname, files);

	for (size_t i = 0; i < files.size(); ++i)
	{
		Mat img = imread(files[i], IMREAD_GRAYSCALE); // load the image
		if (img.empty())            // invalid image, skip it.
		{
			cout << files[i] << " is invalid!" << endl;
			continue;
		}

		if (showImages)
		{
			imshow("image", img);
			waitKey(1);
		}
		img_lst.push_back(img);
	}
}

void sample_neg(const vector< Mat > & full_neg_lst, vector< Mat > & neg_lst, const Size & size)
{
	Rect box;
	box.width = size.width;
	box.height = size.height;

	const int size_x = box.width;
	const int size_y = box.height;

	srand((unsigned int)time(NULL));

	for (size_t i = 0; i < full_neg_lst.size(); i++)
		if (full_neg_lst[i].cols >= box.width && full_neg_lst[i].rows >= box.height)
		{
			box.x = rand() % (full_neg_lst[i].cols - size_x);
			box.y = rand() % (full_neg_lst[i].rows - size_y);
			Mat roi = full_neg_lst[i](box);
			neg_lst.push_back(roi.clone());
		}
}

void computeHOGs(const Size wsize, const vector< Mat > & img_lst, vector< Mat > & gradient_lst, bool use_flip)
{
	HOGDescriptor *hog = new HOGDescriptor(cvSize(28, 28), cvSize(14, 14), cvSize(7, 7), cvSize(7, 7), 9);
	hog->winSize = wsize;
	Mat gray;
	vector< float > descriptors;

	for (size_t i = 0; i < img_lst.size(); i++)
	{
		if (img_lst[i].cols >= wsize.width && img_lst[i].rows >= wsize.height)
		{
			Rect r = Rect((img_lst[i].cols - wsize.width) / 2,
				(img_lst[i].rows - wsize.height) / 2,
				wsize.width,
				wsize.height);
			gray = img_lst[i](r).clone();
			//cvtColor(img_lst[i](r), gray, COLOR_BGR2GRAY);

			hog->compute(gray, descriptors, cv::Size(1, 1), cv::Size(0, 0));
			gradient_lst.push_back(Mat(descriptors).clone());
			if (use_flip)
			{
				flip(gray, gray, 1);
				hog->compute(gray, descriptors, cv::Size(1, 1), cv::Size(0, 0));
				gradient_lst.push_back(Mat(descriptors).clone());
			}
		}
	}
}


int main(int argc, char** argv)
{
	bool train_twice = true;
	bool flip_samples = false;
	Size image_size;
	
	int detector_width = 28;
	int detector_height = 28;

	string buf;
	

	vector<string> img_path;//输入文件名变量  
	vector< Mat > input_list, gradient_lst;
	vector<int> labels;
	int nLine = 0;
	
	ifstream svm_data("data/SVM_DATA.txt");
	while (svm_data)//将训练样本文件依次读取进来    
	{
		if (getline(svm_data, buf))
		{
			nLine++;
			if (nLine % 2 == 0)//注：奇数行是图片全路径，偶数行是标签 
			{
				labels.push_back(atoi(buf.c_str()));//atoi将字符串转换成整型，标志(0,1，2，...，9)，注意这里至少要有两个类别，否则会出错    
			}
			else
			{
				img_path.push_back(buf);//图像路径
				Mat image = imread("data\\" + buf, IMREAD_GRAYSCALE);
				input_list.push_back(image);
			}
		}
	}
	svm_data.close();//关闭文件

	cv::Mat response(labels.size(),1, CV_32S);
	for (int i = 0; i<response.rows; ++i)
		response.at<int>(i, 0) = labels[i];

	clog << "images are being loaded...";


	if (detector_width && detector_height)
	{
		image_size = Size(detector_width, detector_height);
	}

	computeHOGs(image_size,input_list ,gradient_lst, flip_samples);

	Mat train_data;
	convert_to_ml(gradient_lst, train_data);

	clog << "Training SVM...";
	Ptr< SVM > svm = SVM::create();

	//svm->setType(ml::SVM::C_SVC);
	//svm->setKernel(ml::SVM::RBF);
	//svm->setDegree(10.0);
	//svm->setGamma(0.09);
	//svm->setCoef0(1.0);
	//svm->setC(10.0);
	//svm->setNu(0.5);
	//svm->setP(1.0);
	//svm->setTermCriteria(TermCriteria(CV_TERMCRIT_EPS, 1000, FLT_EPSILON));

	/* Default values to train SVM */
	svm->setCoef0(0.0);
	svm->setDegree(10);
	svm->setTermCriteria(TermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 1000, 1e-3));
	svm->setGamma(0.09);
	svm->setKernel(ml::SVM::LINEAR);
//	svm->setKernel(ml::SVM::RBF);
	svm->setNu(0.5);
	svm->setP(0.1); // for EPSILON_SVR, epsilon in loss function?
	svm->setC(0.01); // From paper, soft classifier
	svm->setType(ml::SVM::C_SVC); // C_SVC; // EPSILON_SVR; // may be also NU_SVR; // do regression task
	svm->train(train_data, ROW_SAMPLE, response);
	clog << "...[done]" << endl;
	cout << response << endl;

	//Mat result;
	//Mat img = imread("data/20171203digitron_num/8/2047583278.png",IMREAD_GRAYSCALE);
	//
	////Mat img = Mat::ones(28,28,CV_8UC1);
	//imshow("img", img);
	//waitKey();

	Mat result;
	string test_buf;
	vector<string>testImg_path;//输入文件名变量  
	float sucs_rate;   //正确率
	int count_right = 0;  //识别准确的个数
	int count_false = 0;
	ifstream img_test("data/write_num20171210.txt");
	int testLine = 0;
	while (img_test){
		if (getline(img_test, test_buf))
		{
			testLine++;
			if (testLine % 2 == 0)
			{
				if (atoi(test_buf.c_str()) == result.at<float>(0,0))
					count_right++;
				else
					count_false++;
			}
			else
			{
				testImg_path.push_back(test_buf);//图像路径
				Mat img = imread("data\\" + test_buf, IMREAD_GRAYSCALE);
				vector<Mat> test, hog;
				test.push_back(img);
				computeHOGs(image_size, test, hog, flip_samples);
				Mat hog_data;
				convert_to_ml(hog, hog_data);
				svm->predict(hog_data, result);

				cout << result.at<float>(0,0) << endl;
			}
		}
	}
	img_test.close();

	sucs_rate = (float)(count_right) / (float)(count_false + count_right);

	cout << "count_right" << count_right << endl;
	cout << "count_false" << count_false << endl;
	cout << "success rate:  " << sucs_rate << endl;

	cout << "end  " << endl;

	return 0;
}